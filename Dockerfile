FROM alpine:latest

COPY app/ /app
COPY startup.sh /startup.sh

RUN apk add nodejs nodejs-npm \
    cd /app ; npm install ;\
    apk del nodejs-npm

ENTRYPOINT ["/startup.sh"]
